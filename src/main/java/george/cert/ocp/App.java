package george.cert.ocp;

/**
 * Hello world!
 *
 */

interface I{
    void hello(String s);
}
public class App 
{
    public static void main( String[] args )
    {
        I i = s -> {
            System.out.println(s);
        };
        i.hello("w");
        System.out.println( "Hello World!" );
    }
}
